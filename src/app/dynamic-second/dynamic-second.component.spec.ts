import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicSecondComponent } from './dynamic-second.component';

describe('DynamicSecondComponent', () => {
  let component: DynamicSecondComponent;
  let fixture: ComponentFixture<DynamicSecondComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicSecondComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicSecondComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
