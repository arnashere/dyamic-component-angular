import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicFirstComponent } from './dynamic-first.component';

describe('DynamicFirstComponent', () => {
  let component: DynamicFirstComponent;
  let fixture: ComponentFixture<DynamicFirstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicFirstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicFirstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
