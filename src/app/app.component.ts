import {
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  OnInit,
  Type,
  ViewChild,
  ViewRef
} from '@angular/core';

import { ComponentHostDirective } from './component-host.directive';
import { DynamicFifthComponent } from './dynamic-fifth/dynamic-fifth.component';
import { DynamicFirstComponent } from './dynamic-first/dynamic-first.component';
import { DynamicFourthComponent } from './dynamic-fourth/dynamic-fourth.component';
import { DynamicSecondComponent } from './dynamic-second/dynamic-second.component';
import { DynamicSixthComponent } from './dynamic-sixth/dynamic-sixth.component';
import { DynamicThirdComponent } from './dynamic-third/dynamic-third.component';

interface DetachedComponentType {
  componentReference: ComponentRef<any>;
  viewReference: ViewRef;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'angular9-filterable-table';

  @ViewChild(ComponentHostDirective, { static: true })
  host: ComponentHostDirective;

  components = [
    { id: 1, name: DynamicFirstComponent, active: true },
    { id: 2, name: DynamicSecondComponent, active: false },
    { id: 3, name: DynamicThirdComponent, active: false },
    { id: 4, name: DynamicFourthComponent, active: false },
    { id: 5, name: DynamicFifthComponent, active: false },
    { id: 6, name: DynamicSixthComponent, active: true }
  ];

  componentsAdded: DetachedComponentType[] = [];
  componentsDetached: DetachedComponentType[] = [];

  constructor(private cfr: ComponentFactoryResolver) {}

  ngOnInit() {
    this.loadComponent();
  }

  selectAll() {
    this.components.forEach((component, counter) => {
      if (!component.active) {
        this.addComponent(counter, component.name);
        component.active = true;
      }
    });
  }

  deSelectAll() {
    this.components.forEach((component, counter) => {
      if (component.active) {
        this.removeComponent(counter, component.name);
        component.active = false;
      }
    });
  }

  onchange(event, component, counter) {
    this.components[counter].active = !this.components[counter].active;

    if (this.components[counter].active) {
      this.addComponent(counter, component.name);
    } else {
      this.removeComponent(counter, component.name);
    }
  }

  addComponent(counter, componentClass: Type<any>) {
    const detachedComponent = this.componentsDetached.find((component) => {
      return component.componentReference.instance instanceof componentClass;
    });
    const componentIndex = this.componentsDetached.indexOf(detachedComponent);
    if (componentIndex !== -1) {
      const viewRef = this.host.viewContainerRef.insert(
        detachedComponent.viewReference
      );
      this.componentsAdded.push({
        componentReference: detachedComponent.componentReference,
        viewReference: viewRef
      });
      this.componentsDetached.splice(componentIndex, 1);
    } else {
      // Create component dynamically inside the ng-template
      const componentReference = this.host.viewContainerRef.createComponent(
        this.cfr.resolveComponentFactory(this.components[counter].name)
      );
      // Push the component so that we can keep track of which components are created
      this.componentsAdded.push({
        componentReference,
        viewReference: componentReference.hostView
      });
    }
  }

  removeComponent(counter, componentClass: Type<any>) {
    // Find the component
    const componentReference = this.componentsAdded.find((component) => {
      return component.componentReference.instance instanceof componentClass;
    });

    const componentIndex = this.componentsAdded.indexOf(componentReference);
    if (componentIndex !== -1) {
      // Remove component from both view and array
      const indexAtView = this.host.viewContainerRef.indexOf(
        componentReference.viewReference
      );
      // keep track of the detached component so that it can be added back.
      this.componentsDetached.push({
        componentReference: this.componentsAdded.splice(componentIndex, 1)[0]
          .componentReference,
        viewReference: this.host.viewContainerRef.detach(indexAtView)
      });
    }
  }

  loadComponent() {
    this.host.viewContainerRef.clear();
    let newComponent: ComponentRef<any>;
    this.components.forEach((item) => {
      if (item.active) {
        newComponent = this.host.viewContainerRef.createComponent(
          this.cfr.resolveComponentFactory(item.name)
        );
        // Push both compRef and viewRef so that we can keep track of which components are created
        this.componentsAdded.push({
          componentReference: newComponent,
          viewReference: newComponent.hostView
        });
      }
    });
  }
}
