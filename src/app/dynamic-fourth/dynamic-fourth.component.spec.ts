import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicFourthComponent } from './dynamic-fourth.component';

describe('DynamicFourthComponent', () => {
  let component: DynamicFourthComponent;
  let fixture: ComponentFixture<DynamicFourthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicFourthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicFourthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
