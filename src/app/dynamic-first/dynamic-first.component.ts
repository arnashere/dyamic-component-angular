import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dynamic-first',
  templateUrl: './dynamic-first.component.html',
  styleUrls: ['./dynamic-first.component.scss']
})
export class DynamicFirstComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {
    console.log('first ngOnInit()');
  }
}
