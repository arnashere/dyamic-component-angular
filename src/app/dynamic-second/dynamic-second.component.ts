import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dynamic-second',
  templateUrl: './dynamic-second.component.html',
  styleUrls: ['./dynamic-second.component.scss']
})
export class DynamicSecondComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {
    console.log('second ngOnInit()');
  }
}
