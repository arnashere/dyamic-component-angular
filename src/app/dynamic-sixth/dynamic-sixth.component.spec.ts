import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicSixthComponent } from './dynamic-sixth.component';

describe('DynamicSixthComponent', () => {
  let component: DynamicSixthComponent;
  let fixture: ComponentFixture<DynamicSixthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicSixthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicSixthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
