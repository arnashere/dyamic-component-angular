import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DynamicFirstComponent } from './dynamic-first/dynamic-first.component';
import { DynamicSecondComponent } from './dynamic-second/dynamic-second.component';
import { ComponentHostDirective } from './component-host.directive';
import { DynamicThirdComponent } from './dynamic-third/dynamic-third.component';
import { DynamicFourthComponent } from './dynamic-fourth/dynamic-fourth.component';
import { DynamicFifthComponent } from './dynamic-fifth/dynamic-fifth.component';
import { DynamicSixthComponent } from './dynamic-sixth/dynamic-sixth.component';

@NgModule({
  declarations: [
    AppComponent,
    DynamicFirstComponent,
    DynamicSecondComponent,
    ComponentHostDirective,
    DynamicThirdComponent,
    DynamicFourthComponent,
    DynamicFifthComponent,
    DynamicSixthComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
