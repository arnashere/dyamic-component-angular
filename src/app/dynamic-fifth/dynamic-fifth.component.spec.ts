import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicFifthComponent } from './dynamic-fifth.component';

describe('DynamicFifthComponent', () => {
  let component: DynamicFifthComponent;
  let fixture: ComponentFixture<DynamicFifthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicFifthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicFifthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
