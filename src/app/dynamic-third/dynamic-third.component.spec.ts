import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicThirdComponent } from './dynamic-third.component';

describe('DynamicThirdComponent', () => {
  let component: DynamicThirdComponent;
  let fixture: ComponentFixture<DynamicThirdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicThirdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicThirdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
